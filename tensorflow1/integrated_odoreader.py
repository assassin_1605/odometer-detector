# Import packages
import os
import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import sys
import PIL.Image as Image
import operator
import numpy.core.multiarray
import imutils
from imutils.perspective import four_point_transform
from imutils import contours

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("C:/tensorflow1/models/research")
sys.path.append("C:/tensorflow1/models/research/object_detection")
sys.path.append("C:/tensorflow1/models/research/slim")
# Import utilites
from utils import label_map_util
from utils import visualization_utils as vis_util

# Name of the directory containing the object detection module we're using
MODEL_NAME = 'inference_graph'

#IMAGE_NAME = 'Instrument Panel (46).jpg'

# Grab path to current working directory
CWD_PATH ='C:/tensorflow1/models/research/object_detection'

# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')

# Path to label map file
PATH_TO_LABELS = os.path.join(CWD_PATH,'training','labelmap.pbtxt')

# Path to image

#PATH_TO_IMAGE = os.path.join(CWD_PATH,IMAGE_NAME)

# Number of classes the object detector can identify
NUM_CLASSES = 1

# Load the label map.
# Label maps map indices to category names, so that when our convolution
# network predicts `5`, we know that this corresponds to `king`.
# Here we use internal utility functions, but anything that returns a
# dictionary mapping integers to appropriate string labels would be fine
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Load the Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    sess = tf.Session(graph=detection_graph)

#define blck

def black_image(top_left,bottom_right,image,pth_to_sve):
    height, width = image.shape[:2]
    for i in range(0,height):
        for j in range(0,top_left[0]):
            image[i][j]=0
    for i in range(0,height):
        for j in range(bottom_right[0],width):
            image[i][j]=0
    for i in range(0,top_left[1]):
        for j in range(0,width):
            image[i][j]=0
    for i in range(bottom_right[1],height):
        for j in range(0,width):
            image[i][j]=0
    cv2.imwrite(pth_to_sve,image)
    
# Define input and output tensors (i.e. data) for the object detection classifier

k=1
for i in range(24,27):
        #if i==11:
        #    k=2
        #elif i==16:
        #    k=3

        IMAGE_NAME = 'baleno ('+str(i)+').jpg'
        PATH_TO_IMAGE = os.path.join(CWD_PATH,'imgess',IMAGE_NAME)
        

        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
        detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')

        image = cv2.imread(PATH_TO_IMAGE)
        image_expanded = np.expand_dims(image, axis=0)
        img = Image.open(PATH_TO_IMAGE)

        img_height=image.shape[0]
        img_width=image.shape[1]
    
        (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_expanded})


        for j in range(boxes.shape[1]):
              vis_util.draw_bounding_box_on_image(img,boxes[0,j,0],boxes[0,j,1],boxes[0,j,2],boxes[0,j,3],color='green',thickness=4,display_str_list=(),use_normalized_coordinates=True)
        boxes=np.squeeze(boxes)
        boxes=boxes.reshape(4,-1)
        print(boxes.shape)
        y_min=int(np.ceil(boxes[0,0]*img_height))
        x_min=int(np.ceil(boxes[1,0]*img_width))
        y_max=int(np.ceil(boxes[2,0]*img_height))
        x_max=int(np.ceil(boxes[3,0]*img_width))
        print(y_min,x_min,y_max,x_max)
        #image=image[y_min:y_max,x_min:x_max,:]
        img.show()
        #plt.imshow(image)
        #plt.show()

        if k==1:
            shivam_part='C:/Users/Arijeet/Desktop/sved/shivam/diesel' + str(i) +'.jpg'
            pth_to_sve='C:/Users/Arijeet/Desktop/sved/diesel' + str(i) +'.jpg'
        elif k==2:
            shivam_part='C:/Users/Arijeet/Desktop/sved/shivam/petrol' + str(i) +'.jpg'
            pth_to_sve='C:/Users/Arijeet/Desktop/sved/petrol' + str(i)+'.jpg'
        elif k==3:
            shivam_part='C:/Users/Arijeet/Desktop/sved/shivam/verna' + str(i) +'.jpg'
            pth_to_sve='C:/Users/Arijeet/Desktop/sved/verna' + str(i)+ '.jpg'
        
                    
        top_left=[x_min,y_min]
        bottom_right=[x_max,y_max]
        black_image(top_left,bottom_right,image,pth_to_sve)


        #***************************shivam************************************
# module level variables ##########################################################################
        MIN_CONTOUR_AREA = 2955

        RESIZED_IMAGE_WIDTH = 20
        RESIZED_IMAGE_HEIGHT = 30

    ###################################################################################################
        class ContourWithData():

            # member variables ############################################################################
            npaContour = None           # contour
            boundingRect = None         # bounding rect for contour
            intRectX = 0                # bounding rect top left corner x location
            intRectY = 0                # bounding rect top left corner y location
            intRectWidth = 0            # bounding rect width
            intRectHeight = 0           # bounding rect height
            fltArea = 0.0               # area of contour

            def calculateRectTopLeftPointAndWidthAndHeight(self):               # calculate bounding rect info
                [intX, intY, intWidth, intHeight] = self.boundingRect
                self.intRectX = intX
                self.intRectY = intY
                self.intRectWidth = intWidth
                self.intRectHeight = intHeight

            def checkIfContourIsValid(self):                            # this is oversimplified, for a production grade program
                if self.fltArea < MIN_CONTOUR_AREA: return False        # much better validity checking would be necessary
                return True

        ###################################################################################################
        def main():
            allContoursWithData = []                # declare empty lists,
            validContoursWithData = []              # we will fill these shortly

            try:
                npaClassifications = np.loadtxt("C:/Users/Arijeet/Desktop/integration/classifications.txt", np.float32)                  # read in training classifications
            except:
                print ("error, unable to open classifications.txt, exiting program\n")
                os.system("pause")
                return
            # end try

            try:
                npaFlattenedImages = np.loadtxt("C:/Users/Arijeet/Desktop/integration/flattened_images.txt", np.float32)                 # read in training images
            except:
                print ("error, unable to open flattened_images.txt, exiting program\n")
                os.system("pause")
                return
            # end try

            npaClassifications = npaClassifications.reshape((npaClassifications.size, 1))       # reshape numpy array to 1d, necessary to pass to call to train

            kNearest = cv2.ml.KNearest_create()                   # instantiate KNN object

            kNearest.train(npaFlattenedImages, cv2.ml.ROW_SAMPLE, npaClassifications)

            imgTestingNumbers = cv2.imread(pth_to_sve)          # read in testing numbers image

            if imgTestingNumbers is None:                           # if image was not read successfully
                print ("error: image not read from file \n\n")        # print error message to std out
                os.system("pause")                                  # pause so user can see error message
                return                                              # and exit function (which exits program)
            # end if

            imgGray = cv2.cvtColor(imgTestingNumbers, cv2.COLOR_BGR2GRAY)       # get grayscale image
            imgBlurred = cv2.GaussianBlur(imgGray, (5,5), 0)                    # blur

                                                                # filter image from grayscale to black and white
            imgThresh = cv2.adaptiveThreshold(imgBlurred,                           # input image
                                              255,                                  # make pixels that pass the threshold full white
                                              cv2.ADAPTIVE_THRESH_GAUSSIAN_C,       # use gaussian rather than mean, seems to give better results
                                              cv2.THRESH_BINARY_INV,                # invert so foreground will be white, background will be black
                                              11,                                   # size of a pixel neighborhood used to calculate threshold value
                                              2)                                    # constant subtracted from the mean or weighted mean

            imgThreshCopy = imgThresh.copy()        # make a copy of the thresh image, this in necessary b/c findContours modifies the image

            imgContours, npaContours, npaHierarchy = cv2.findContours(imgThreshCopy,             # input image, make sure to use a copy since the function will modify this image in the course of finding contours
                                                         cv2.RETR_LIST,         # retrieve the outermost contours only
                                                         cv2.CHAIN_APPROX_NONE)   # compress horizontal, vertical, and diagonal segments and leave only their end points
            

            
            #mask = np.zeros_like(imgTestingNumbers)
            #cv2.drawContours(mask, npaContours, 62, 255, -1)
            #out = np.zeros_like(imgTestingNumbers) # Extract out the object and place into output image
            #out[mask == 255] = imgTestingNumbers[mask == 255]

            # Show the output image
            #cv2.imshow('Output', out)
            #cv2.waitKey(0)
            #cv2.destroyAllWindows()

            for npaContour in npaContours:                             # for each contour
                contourWithData = ContourWithData()                                             # instantiate a contour with data object
                contourWithData.npaContour = npaContour                                         # assign contour to contour with data
                contourWithData.boundingRect = cv2.boundingRect(contourWithData.npaContour)     # get the bounding rect
                
                contourWithData.calculateRectTopLeftPointAndWidthAndHeight()                    # get bounding rect info
                contourWithData.fltArea = cv2.contourArea(contourWithData.npaContour)           # calculate the contour area
                allContoursWithData.append(contourWithData)                                     # add contour with data object to list of all contours with data
            # end for

            for contourWithData in allContoursWithData:                 # for all contours
                if contourWithData.checkIfContourIsValid():             # check if valid
                    validContoursWithData.append(contourWithData)       # if so, append to valid contour list
                # end if
            # end for

            #validContoursWithData.sort(key = operator.attrgetter("intRectX"))         # sort contours from left to right

            strFinalString = ""         # declare final string, this will have the final number sequence by the end of the program
            

            for contourWithData in validContoursWithData:            # for each contour
                                                        # draw a green rect around the current char
                cv2.rectangle(imgTestingNumbers,                                        # draw rectangle on original testing image
                              (contourWithData.intRectX, contourWithData.intRectY),     # upper left corner
                              (contourWithData.intRectX + contourWithData.intRectWidth, contourWithData.intRectY + contourWithData.intRectHeight),      # lower right corner
                              (0, 255, 0),              # green
                              2)                        # thickness
                
                a=contourWithData.intRectX
                b=contourWithData.intRectY
                c=(contourWithData.intRectX + contourWithData.intRectWidth)
                d=(contourWithData.intRectY + contourWithData.intRectHeight)
                cv2.imshow("first_contour_image",imgTestingNumbers)
                cv2.waitKey(0)

                stencil = np.zeros(imgTestingNumbers.shape).astype(imgTestingNumbers.dtype)
                counter = [np.array([[a+(c-a)/2, b+(d-b)/2],[a+(c-a)/2,d] ,[c, d], [c, b+(d-b)/2]])]
                color = (255,255,255)
                cv2.fillPoly(stencil, np.int32(counter), color)
                result = cv2.bitwise_and(imgTestingNumbers, stencil)
                cv2.imshow("first_contour_image.jpg",result)
                cv2.waitKey(0)
                cv2.imwrite(shivam_part, result)
                break
                

                #mask = np.zeros_like(imgTestingNumbers)
                #out = np.zeros_like(imgTestingNumbers) # Extract out the object and place into output image
                #out[mask == 255] = imgTestingNumbers[mask == 255]

                #(x, y) = np.where(mask == 255)
                #(topx, topy) = (np.min(x), np.min(y))
                #(bottomx, bottomy) = (np.max(x), np.max(y))
                #out = out[topx:bottomx+1, topy:bottomy+1]
                #cv2.imshow('Output', out)
                #cv2.waitKey(0)
                #cv2.destroyAllWindows()

                imgROI = imgThresh[contourWithData.intRectY : contourWithData.intRectY + contourWithData.intRectHeight,     # crop char out of threshold image
                                   contourWithData.intRectX : contourWithData.intRectX + contourWithData.intRectWidth]


                imgROIResized = cv2.resize(imgROI, (RESIZED_IMAGE_WIDTH, RESIZED_IMAGE_HEIGHT))             # resize image, this will be more consistent for recognition and storage

                npaROIResized = imgROIResized.reshape((1, RESIZED_IMAGE_WIDTH * RESIZED_IMAGE_HEIGHT))      # flatten image into 1d numpy array

                npaROIResized = np.float32(npaROIResized)       # convert from 1d numpy array of ints to 1d numpy array of floats

                retval, npaResults, neigh_resp, dists = kNearest.findNearest(npaROIResized, k = 1)     # call KNN function find_nearest

                strCurrentChar = str(chr(int(npaResults[0][0])))                                             # get character from results

                strFinalString = strFinalString + strCurrentChar            # append current char to full string
            # end for

            print ("\n" + strFinalString + "\n")                  # show the full string

            #cv2.imshow("imgTestingNumbers", imgTestingNumbers)      # show input image with green boxes drawn around found digits
            #cv2.waitKey(0)                                          # wait for user key press

            cv2.destroyAllWindows()             # remove windows from memory

            return

        ###################################################################################################
        if __name__ == "__main__":
            main()
        # end if

               






        #***************************rishabh***********************************
        a=[]
        #######   training part    ###############
        samples = np.loadtxt('C:/Users/Arijeet/Desktop/integration/generalsamples.csv',np.float32)
        responses = np.loadtxt('C:/Users/Arijeet/Desktop/integration/generalresponses.csv',np.float32)
        responses = responses.reshape((responses.size,1))

        model = cv2.ml.KNearest_create()
        model.train(samples,cv2.ml.ROW_SAMPLE,responses)

        ############################# testing part  #########################

        image = cv2.imread(shivam_part)

        #image = imutils.resize(image, height=500)
        #cv2.imwrite('image1.jpg',image)
        height, width = image.shape[:2]
        #print(str(height) + " " + str(width))
        out = np.zeros(image.shape,np.uint8)
        gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)

        blur = cv2.GaussianBlur(gray,(5,5),0)
        thresh = cv2.adaptiveThreshold(blur,255,1,1,11,2)
        '''cv2.imshow("image", image)
        cv2.waitKey(0)
        out = np.zeros(image.shape,np.uint8)
        gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        edged = cv2.adaptiveThreshold(gray,255,1,1,11,5)
        #edged= cv2.adaptiveThreshold(image,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,1)
        cv2.imshow("edged",edged)
        cv2.waitKey(0)'''
        '''image=cv2.imread('odm.jpg')
        image = imutils.resize(image, height=500)
        out = np.zeros(image.shape,np.uint8)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        edged = cv2.Canny(blurred, 50, 200, 255)
        cv2.imshow("ads",edged)'''

        _,contours,hierarchy = cv2.findContours(thresh,cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)

        for cnt in contours:
            if cv2.contourArea(cnt)>15 :
                [x,y,w,h] = cv2.boundingRect(cnt)
                if h>w :
                    cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),1)
                    roi = thresh[y:y+h,x:x+w]
                    roismall = cv2.resize(roi,(10,10))
                    roismall = roismall.reshape((1,100))
                    roismall = np.float32(roismall)
                    retval, results, neigh_resp, dists = model.findNearest(roismall, k = 3)
                    string = str(int((results[0][0])))
                    #print(string)
                    a.append(int(string))
                    cv2.putText(out,string,(x,y+h),0,1,(0,255,0))

        '''for cnt in contours:
            if cv2.contourArea(cnt)>0:
                [x,y,w,h] = cv2.boundingRect(cnt)
                if  h>(20/500 *height) and h<(60/500 * height):
                    if y>(40) and y<(51):
                        cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),1)
                        roi = edged[y:y+h,x:x+w]
                        roismall = cv2.resize(roi,(10,10))
                        roismall = roismall.reshape((1,100))
                        roismall = np.float32(roismall)
                        retval, results, neigh_resp, dists = model.findNearest(roismall, k = 1)
                        string = str(int((results[0][0])))
                        print(string)
                        cv2.putText(out,string,(x,y+h),0,1,(0,255,0))'''
        cv2.imshow('im',image)
        cv2.imshow('out',out)

        
        print ("odometer reading "+ ''.join(map(str, a)))
        cv2.waitKey(0)



