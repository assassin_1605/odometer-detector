######## Image Object Detection Using Tensorflow-trained Classifier #########
#
# Author: Evan Juras
# Date: 1/15/18
# Description: 
# This program uses a TensorFlow-trained classifier to perform object detection.
# It loads the classifier uses it to perform object detection on an image.
# It draws boxes and scores around the objects of interest in the image.

## Some of the code is copied from Google's example at
## https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb

## and some is copied from Dat Tran's example at
## https://github.com/datitran/object_detector_app/blob/master/object_detection_app.py

## but I changed it to make it more understandable to me.

# Import packages
import os
import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import sys
import PIL.Image as Image

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("C:/tensorflow1/models/research")
sys.path.append("C:/tensorflow1/models/research/object_detection")
sys.path.append("C:/tensorflow1/models/research/slim")
# Import utilites
from utils import label_map_util
from utils import visualization_utils as vis_util

# Name of the directory containing the object detection module we're using
MODEL_NAME = 'inference_graph'

#IMAGE_NAME = 'Instrument Panel (46).jpg'

# Grab path to current working directory
CWD_PATH ='C:/tensorflow1/models/research/object_detection'

# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')

# Path to label map file
PATH_TO_LABELS = os.path.join(CWD_PATH,'training','labelmap.pbtxt')

# Path to image

#PATH_TO_IMAGE = os.path.join(CWD_PATH,IMAGE_NAME)

# Number of classes the object detector can identify
NUM_CLASSES = 1

# Load the label map.
# Label maps map indices to category names, so that when our convolution
# network predicts `5`, we know that this corresponds to `king`.
# Here we use internal utility functions, but anything that returns a
# dictionary mapping integers to appropriate string labels would be fine
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Load the Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    sess = tf.Session(graph=detection_graph)

#define blck

def black_image(top_left,bottom_right,image,pth_to_sve):
    height, width = image.shape[:2]
    for i in range(0,height):
        for j in range(0,top_left[0]):
            image[i][j]=0
    for i in range(0,height):
        for j in range(bottom_right[0],width):
            image[i][j]=0
    for i in range(0,top_left[1]):
        for j in range(0,width):
            image[i][j]=0
    for i in range(bottom_right[1],height):
        for j in range(0,width):
            image[i][j]=0
    cv2.imwrite(pth_to_sve,image)
    
# Define input and output tensors (i.e. data) for the object detection classifier

k=4
for i in range(102,133):
        if i==58:
            k=2
        elif i==71:
            k=3
        elif i==102:
            k=4

        if k==1:
            IMAGE_NAME ='C:/tensorflow1/models/research/object_detection/imges/diesel (' + str(i) +').jpg'
            pth_to_sve='C:/Users/Arijeet/Desktop/sved/diesel (' + str(i) +').jpg'
        elif k==2:
            IMAGE_NAME ='C:/tensorflow1/models/research/object_detection/imges/petrol (' + str(i-57) +').jpg'
            pth_to_sve='C:/Users/Arijeet/Desktop/sved/petrol (' + str(i-57)+').jpg'
        elif k==3:
            IMAGE_NAME ='C:/tensorflow1/models/research/object_detection/imges/verna (' + str(i-70) +').jpg'
            pth_to_sve='C:/Users/Arijeet/Desktop/sved/verna (' + str(i-70)+ ').jpg'
        else:
            IMAGE_NAME ='C:/tensorflow1/models/research/object_detection/imges/baleno (' + str(i-101) +').jpg'
            pth_to_sve='C:/Users/Arijeet/Desktop/sved/baleno (' + str(i-101)+ ').jpg'
         
        PATH_TO_IMAGE = os.path.join(CWD_PATH,'imges',IMAGE_NAME)
        

        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
        detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')

        image = cv2.imread(PATH_TO_IMAGE)
        image_expanded = np.expand_dims(image, axis=0)
        img = Image.open(PATH_TO_IMAGE)

        img_height=image.shape[0]
        img_width=image.shape[1]
    
        (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_expanded})


        for j in range(boxes.shape[1]):
              vis_util.draw_bounding_box_on_image(img,boxes[0,j,0],boxes[0,j,1],boxes[0,j,2],boxes[0,j,3],color='green',thickness=4,display_str_list=(),use_normalized_coordinates=True)
        boxes=np.squeeze(boxes)
        boxes=boxes.reshape(4,-1)
        print(boxes.shape)
        y_min=int(np.ceil(boxes[0,0]*img_height))
        x_min=int(np.ceil(boxes[1,0]*img_width))
        y_max=int(np.ceil(boxes[2,0]*img_height))
        x_max=int(np.ceil(boxes[3,0]*img_width))
        print(y_min,x_min,y_max,x_max)
        #image=image[y_min:y_max,x_min:x_max,:]
        img.show()
        #plt.imshow(image)
        #plt.show()

                
        top_left=[x_min,y_min]
        bottom_right=[x_max,y_max]
        #black_image(top_left,bottom_right,image,pth_to_sve)

# Input tensor is the image
#image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

# Output tensors are the detection boxes, scores, and classes
# Each box represents a part of the image where a particular object was detected
#detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

# Each score represents level of confidence for each of the objects.
# The score is shown on the result image, together with the class label.
#detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
#detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

# Number of objects detected
#num_detections = detection_graph.get_tensor_by_name('num_detections:0')

# Load image using OpenCV and
# expand image dimensions to have shape: [1, None, None, 3]
# i.e. a single-column array, where each item in the column has the pixel RGB value

#image = cv2.imread(PATH_TO_IMAGE)
#image_expanded = np.expand_dims(image, axis=0)
#img = Image.open(PATH_TO_IMAGE)

# Perform the actual detection by running the model with the image as input

#(boxes, scores, classes, num) = sess.run(
#    [detection_boxes, detection_scores, detection_classes, num_detections],
#   feed_dict={image_tensor: image_expanded})
#print(boxes)
#print(classes)

# Draw the results of the detection (aka 'visulaize the results')

#for i in range(boxes.shape[1]):
#   vis_util.draw_bounding_box_on_image(img,boxes[0,i,0],boxes[0,i,1],boxes[0,i,2],boxes[0,i,3],color='green',thickness=4,display_str_list=(),use_normalized_coordinates=True)
#boxes=np.squeeze(boxes)
#print(boxes.shape)
#img.show()

"""vis_util.visualize_boxes_and_labels_on_image_array(
    image,
    np.squeeze(boxes),
    np.squeeze(classes).astype(np.int32),
    np.squeeze(scores),
    category_index,
    use_normalized_coordinates=True,
    line_thickness=8,
    min_score_thresh=0.80)

# All the results have been drawn on image. Now display the image.
cv2.imshow('Object detector', image)

# Press any key to close the image
cv2.waitKey(0)

# Clean up
cv2.destroyAllWindows()
"""
